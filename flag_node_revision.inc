<?php
// $Id: flag_terms.inc,v 1.3 2009/11/19 13:09:02 tayzlor Exp $
/**
 * @file
 * flag_terms.inc
 * Written by Graham Taylor.
 */
 
/**
 * Implements a term flag.
 */
class flag_node_revision extends flag_flag {
  
  function options() {
    $options = parent::options();
    $options += array(
      'revision_per_node' => 0,
    );
    return $options;
  }

  function options_form(&$form) {
    parent::options_form($form);
    
    $form['revision_per_node'] = array(
      '#type' => 'radios',
      '#title' => t('Flag applied to multiple revisions of a node?'),
      '#options' => array(
        '0' => t('Multiple revisions of a node may be flagged'),
        '1' => t('Only one revision of a node may be flagged'),
      ),
      '#default_value' => $this->revision_per_node,
      '#description' => t('If set to multiple, flags are added to revisions without checking to see if another revision on the node has already been flagged. If set to single, flagging a revision will only allow a single revision of a node to be flagged, and will unflag any other flagged revision it finds.'),
    );
  }
  
  /**
   * Flags, or unflags, an item.
   *
   * @param $action
   *   Either 'flag' or 'unflag'.
   * @param $content_id
   *   The ID of the item to flag or unflag.
   * @param $account
   *   The user on whose behalf to flag. Leave empty for the current user.
   * @param $skip_permission_check
   *   Flag the item even if the $account user don't have permission to do so.
   * @return
   *   FALSE if some error occured (e.g., user has no permission, flag isn't
   *   applicable to the item, etc.), TRUE otherwise.
   *
   * flag_node_revision::flag - Implement revision_per_node handling...
   */
  function flag($action, $content_id, $account = NULL, $skip_permission_check = FALSE) {
    if ($action == 'flag' && $this->revision_per_node) {
      // only one flag per node... at the moment, we only correctly handle global flags
      $this->remove_flags_nid($content_id, $account, $skip_permission_check);
    }
    return parent::flag($action, $content_id, $account, $skip_permission_check);
  }
  
  
  /**
   * @param int $content_id Identifier for a content to clear flags for
   * @param user $account User account object
   *
   * Deletes all flags for a user/node
   */
  function remove_flags_nid($content_id, $account = null, $skip_permission_check = false) {
    $nid = $this->get_nid($content_id);
    $revisions = $this->get_flagged_version($nid, true);
    if (is_array($revisions)) {
      foreach ($revisions as $revision_id) {
        $this->flag('unflag', $revision_id, $account, $skip_permission_check);
      }
    }
  }
  
  function _load_content($content_id) {
    // need to find the appropriate node for this revision
    $nid = db_result(db_query('SELECT nid FROM node_revisions WHERE vid = %s', $content_id));
    return node_load($nid, $content_id);
  }

  function applies_to_content_object($node) {
    if ($node && in_array($node->type,$this->types)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * get_content_id($node)
   *
   */
  function get_content_id($node) {
    return $node->vid;
  }
  
  /**
   * get_nid($content_id)
   *
   * @param int $content_id - id for a flagged revision
   *
   * @return int $nid - node id for flagged node
   */
  function get_nid($content_id) {
    if ($content_id) {
      $result = db_result(db_query('SELECT nid FROM {node_revisions} WHERE vid = %d', $content_id));
      return $result;
    }
  }

  // The 'Token' module doesn't seem to provide any term tokens, so
  // the following two methods aren't really useful.

  function get_labels_token_types() {
    return array('node_revision');
  }


  function get_flag_action($content_id) {
    $flag_action = parent::get_flag_action($content_id);
    $node = $this->fetch_content($content_id);
    $flag_action->content_title = $revision->name. ' ' . $this->get_title();
    $flag_action->content_url = _flag_url('node/'.$node->nid.'/revisions/' . $node->vid);
    return $flag_action;
  }

  function get_relevant_action_objects($content_id) {
    return array(
      'node_revision' => $this->fetch_content($content_id),
    );
  }

  function rules_get_event_arguments_definition() {
    return array(
      'term' => array(
        'type' => 'node_revision',
        'label' => t('flagged revision'),
        'handler' => 'flag_rules_get_event_argument',
      ),
    );
  }

  function rules_get_element_argument_definition() {
    return array('type' => 'node_revision', 'label' => t('Flagged node revision'));
  }
  
  function get_views_info() {
    return array(
      'views table' => 'node_revisions',
      'join field' => 'vid',
      'title field' => 'name',
      'title' => t('Node Revision flag'),
      'help' => t('Limit results to only those terms flagged by a certain flag; Or display information about the flag set on a node.'),
      'counter title' => t('Node Revision flag counter'),
      'counter help' => t('Include this to gain access to the flag counter field.'),

    );
  }
  
  /**
   * @param int $nid - Node ID to find revisions for
   * @param bool $all - by default, returns a single result if revision_per_node is set. Set this param to true to get all results.
   *
   * given a nid, return the flagged vid, or null if no version flagged
   */
  function get_flagged_version($nid, $all = false) {
    $qry = 'SELECT content_id FROM flag_content WHERE fid = %d AND content_id IN (SELECT vid FROM node_revisions WHERE nid = %d)';
    $result = db_query($qry, $this->fid, $nid);
    if ($result) {
      if ($this->revision_per_node && !$all) {
        return db_result($result);
      } else {
        $rows = array();
        while ($row = db_result($result)) {
          $rows[] = $row;
        }
        return $rows;
      }
    }
  }

  function applies_to_content_id_array($content_ids) {
    $passed = array();
    foreach ($content_ids as $vid) {
      if ($this->applies_to_content_id($vid)) {
        $passed[$vid] = TRUE;
      }
    }
    return $passed;
  }
}